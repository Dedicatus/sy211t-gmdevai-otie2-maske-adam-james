using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour
{
    GameObject[] goalLocations;
    NavMeshAgent agent;

    Animator animator;

    float speedMultiplier;
    float detectionRadius = 10.0f;
    float fleeRadius = 10.0f;

    private void ResetAgent()
    {
        speedMultiplier = Random.Range(0.3f, 1.5f);
        agent.speed = 2 * speedMultiplier;
        agent.angularSpeed = 120;
        animator.SetFloat("speedMultiplier", speedMultiplier);
        animator.SetTrigger("isWalking");
        agent.ResetPath();
    }

    // Start is called before the first frame update
    void Start()
    {
        goalLocations = GameObject.FindGameObjectsWithTag("goal");
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        //sets the agent's destination to a random goal
        agent.SetDestination(goalLocations[Random.Range(0, goalLocations.Length)].transform.position);

        //animates the agent's walking animation
        animator.SetFloat("walkOffset", Random.Range(0.1f, 1.0f));

        ResetAgent();
        /* code moved to the ResetAgent function
        animator.SetFloat("speedMultiplier", speedMultiplier);
        agent.speed = 2 * speedMultiplier;
        animator.SetTrigger("isWalking");
        speedMultiplier = Random.Range(0.3f, 1.5f);
        */
    }

    // Update is called once per frame
    void Update()
    {
        if(agent.remainingDistance < 1)
        {
            ResetAgent();
            agent.SetDestination(goalLocations[Random.Range(0, goalLocations.Length)].transform.position);
        }
    }

    public void DetectNewMonster(Vector3 location)
    {
        //checks if the agent is in range of the monster
        if(Vector3.Distance(location, this.transform.position) < detectionRadius)
        {
            Vector3 fleeDirection = (this.transform.position - location).normalized;
            Vector3 newGoal = this.transform.position + fleeDirection * fleeRadius;

            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(newGoal, path);

            //check to see if the path is valid
            if(path.status != NavMeshPathStatus.PathInvalid)
            {
                agent.SetDestination(path.corners[path.corners.Length - 1]);
                animator.SetTrigger("isRunning");
                agent.speed = 10;
                agent.angularSpeed = 500;
            }
        }
    }

    public void DetectNewTarget(Vector3 location)
    {
        //checks if the agent is in range of the monster
        if (Vector3.Distance(location, this.transform.position) < detectionRadius)
        {
            Vector3 newGoal = location;

            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(newGoal, path);

            //check to see if the path is valid
            if (path.status != NavMeshPathStatus.PathInvalid)
            {
                agent.SetDestination(path.corners[path.corners.Length - 1]);
                animator.SetTrigger("isRunning");
                agent.speed = 10;
                agent.angularSpeed = 500;
            }
        }
    }
}
