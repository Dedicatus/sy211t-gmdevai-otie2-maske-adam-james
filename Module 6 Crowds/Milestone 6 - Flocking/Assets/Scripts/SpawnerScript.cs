using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{
    GameObject[] agents;
    public GameObject monster;
    public GameObject target;
    
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("agent");
    }

    // Update is called once per frame
    void Update()
    {
        //Spawning the monster
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;

            //finds the area where the mouse is clicked
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                //spawns the monster where the mouse is clicked
                Instantiate(monster, hit.point, monster.transform.rotation);

                foreach (GameObject a in agents)
                {
                    a.GetComponent<AIControl>().DetectNewMonster(hit.point);
                }
            }
        }

        //Spawning the new goal
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;

            //finds the area where the mouse is clicked
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                //spawns the monster where the mouse is clicked
                Instantiate(target, hit.point, target.transform.rotation);

                foreach (GameObject a in agents)
                {
                    a.GetComponent<AIControl>().DetectNewTarget(hit.point);
                }
            }
        }
    }
}
