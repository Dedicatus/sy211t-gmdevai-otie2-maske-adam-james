using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollow : MonoBehaviour
{
    //public GameObject[] waypoints;
    //public UnityStandardAssets.Utility.WaypointCircuit circuit;
    //int currentWaypointIndex = 0;
    //public float stopDistance;

    public Transform objectToFollow;

    float movementSpeed;
    public float acceleration;
    public float deceleration;
    public float rotationSpeed;
    
    public float minSpeed;
    public float maxSpeed;
    public float breakAngle;

    // Start is called before the first frame update
    void Start()
    {
        //waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
    }

    // Update is called once per frame
    void Update()
    {
        

        //Get direction of next waypoint
        Vector3 Direction = new Vector3(objectToFollow.position.x, transform.position.y, objectToFollow.position.z) - this.transform.position;

        //Slerp Rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Direction), rotationSpeed * Time.deltaTime);

        //Car Movement
        if (Vector3.Angle(objectToFollow.forward, transform.forward) > breakAngle && movementSpeed > 0.01){
            movementSpeed = Mathf.Clamp(movementSpeed - (deceleration * Time.deltaTime), minSpeed, maxSpeed);
        }
        else{
            movementSpeed = Mathf.Clamp(movementSpeed + (acceleration * Time.deltaTime), minSpeed, maxSpeed);
        }
        this.transform.Translate(0, 0, movementSpeed);

        /*
        if(circuit.Waypoints.Length == 0){
            return;
        }
        */

        //Follow Waypoint System Code:

        //GameObject currentWaypoint = circuit.Waypoints[currentWaypointIndex].gameObject;

        //Get the direction of the next waypoint
        //Vector3 Direction = new Vector3(currentWaypoint.transform.position.x, transform.position.y, currentWaypoint.transform.position.z) - this.transform.position;

        //car goes forward until it is close enough
        /*
        if (Direction.magnitude > stopDistance)
        {
            //transform.position+=(transform.forward * (movementSpeed * Time.deltaTime));
            transform.position = Vector3.Lerp(transform.position, transform.position + transform.forward, movementSpeed * Time.deltaTime);
        }
        */

        //Tells the car to go to the next waypoint
        /*
        if(Direction.magnitude <= stopDistance)
        {
            currentWaypointIndex++;
            if(currentWaypointIndex >= circuit.Waypoints.Length)
            {
                currentWaypointIndex = 0;
            }
        }
        */
    }
}
