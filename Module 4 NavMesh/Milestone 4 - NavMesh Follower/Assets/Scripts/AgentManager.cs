using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    GameObject[] agents;
    Vector3 playerPosition;
    public float stoppingDistance = 10.0f;
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("AI");
        //playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;

        //tell the agents to go to the player's position
        foreach (GameObject ai in agents) {
            ai.GetComponent<AIControl>().agent.SetDestination(playerPosition);

            //stops the AI when it's close enough to the player
            if((ai.transform.position - playerPosition).magnitude <= stoppingDistance)
            {
                ai.GetComponent<AIControl>().agent.isStopped = true;
            } else ai.GetComponent<AIControl>().agent.isStopped = false;
        }

        Debug.Log(agents.Length);
    }
}