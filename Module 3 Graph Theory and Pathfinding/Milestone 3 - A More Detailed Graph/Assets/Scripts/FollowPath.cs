using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    Transform goal;
    float movementSpeed = 8.0f;
    float rotationSpeed = 3.0f;
    float accuracy = 1.0f;
    public GameObject wpManager;
    GameObject[] wps;
    GameObject currentNode;
    int currentWaypointIndex;
    Graph graph;

    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent<WaypointManager>().graph;
        currentNode = wps[0];
    }

    // Update is called once per frame
    void Update()
    {
        if(graph.getPathLength() == 0 || currentWaypointIndex == graph.getPathLength())
        {
            return;
        }

        //the node we're closest to at the moment
        currentNode = graph.getPathPoint(currentWaypointIndex);

        //if we're close enough to the current waypoint, move on to the next one
        if(Vector3.Distance(graph.getPathPoint(currentWaypointIndex).transform.position, transform.position) < accuracy)
        {
            currentWaypointIndex++;
        }

        //if we are not at the end of the path
        if (currentWaypointIndex < graph.getPathLength())
        {
            goal = graph.getPathPoint(currentWaypointIndex).transform;
            Vector3 lookatGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
            Vector3 direction = lookatGoal - this.transform.position;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);
            this.transform.Translate(0, 0, movementSpeed * Time.deltaTime);
        }

        
    }

    //Go To Functions
    public void GoToTwinMountains()
    {
        int waypointNumber = 0;
        graph.AStar(currentNode, wps[waypointNumber]);
        currentWaypointIndex = 0;
    }

    public void GoToBarracks()
    {
        int waypointNumber = 1;
        graph.AStar(currentNode, wps[waypointNumber]);
        currentWaypointIndex = 0;
    }

    public void GoToCommandCenter()
    {
        int waypointNumber = 2;
        graph.AStar(currentNode, wps[waypointNumber]);
        currentWaypointIndex = 0;
    }

    public void GoToOilRefinery()
    {
        int waypointNumber = 3;
        graph.AStar(currentNode, wps[waypointNumber]);
        currentWaypointIndex = 0;
    }

    public void GoToTankers()
    {
        int waypointNumber = 4;
        graph.AStar(currentNode, wps[waypointNumber]);
        currentWaypointIndex = 0;
    }

    public void GoToRadar()
    {
        int waypointNumber = 5;
        graph.AStar(currentNode, wps[waypointNumber]);
        currentWaypointIndex = 0;
    }
    public void GoToCommandPost()
    {
        int waypointNumber = 6;
        graph.AStar(currentNode, wps[waypointNumber]);
        currentWaypointIndex = 0;
    }
    public void GoToMiddle()
    {
        int waypointNumber = 7;
        graph.AStar(currentNode, wps[waypointNumber]);
        currentWaypointIndex = 0;
    }
}
