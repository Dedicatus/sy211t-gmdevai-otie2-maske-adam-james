using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetAI : MonoBehaviour
{
    Transform Goal;
    GameObject ObjectToFollow;
    public float stopDistance;
    public float movementSpeed;
    public float rotationSpeed;

    // Start is called before the first frame update
    void Awake()
    {
        //Gets the player character and sets it to be the object the pet will follow
        ObjectToFollow = GameObject.FindGameObjectWithTag("Player");
        Goal = ObjectToFollow.transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 Direction = new Vector3(Goal.position.x, transform.position.y, Goal.position.z) - this.transform.position;

        //Slerp Rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Direction), rotationSpeed * Time.deltaTime);

        //pet goes forward until it is close enough
        if(Direction.magnitude > stopDistance){
            //transform.position+=(transform.forward * (movementSpeed * Time.deltaTime));
            transform.position = Vector3.Lerp(transform.position, transform.position + transform.forward, movementSpeed * Time.deltaTime);
        }
    }
}
